package xxx.core.screens.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import xxx.R;
import xxx.asn.CountryPhoneCode;
import xxx.asn.RosterObjectHydrator;
import xxx.core.xxxViewModel;
import xxx.core.yyyApplication;
import xxx.core.api.BaseAsyncCallback;
import xxx.core.api.zzzApi;
import xxx.core.events.SmsReceivedEvent;
import xxx.core.helpers.PreferenceHelper;
import xxx.core.helpers.Utils;
import xxx.core.helpers.analytics.AnalyticManagerClient;
import xxx.core.realm.objects.ProfileRealm;
import xxx.core.screens.editprofile.ProfileCreateActivity;
import xxx.core.screens.login.country.select.SelectCountryActivity;
import xxx.view.xxxEditTextValidationHelper;

import de.greenrobot.event.EventBus;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import timber.log.Timber;
import zzz.objects.roster.EnumConstants;
import zzz.objects.roster.ErrorResp;
import zzz.objects.roster.RequestVerification;
import zzz.objects.roster.RequestVerificationResp;
import zzz.objects.roster.UserResp;

/**
 * ViewModel for authentication
 */
public class LoginViewModel extends xxxViewModel {

	/**
	 * Country code of Ukraine
	 */
	private final static String UKRAINE_LETTER = "UA";

	/**
	 * Frequency of sms sending
	 */
	private final static long SEND_SMS_COUNT = 2 * 60 * 1000;

	/**
	 * Country selected request code
	 */
	static final int COUNTRY_REQUEST_CODE = 105;

	/**
	 * Emits the validity of phone number
	 */
	public ObservableBoolean isPhoneNumberValid = new ObservableBoolean(false);

	public ObservableBoolean isSendSmsEnabled = isPhoneNumberValid;

	/**
	 * Emits a value if the code was sent or not
	 */
	public ObservableBoolean isCodeSent = new ObservableBoolean(false);

	/**
	 * Emits the phone that user enters
	 */
	public ObservableField<String> phone = new ObservableField<>("");

	/**
	 * Emits the mask of the field
	 */
	public ObservableField<String> mask = new ObservableField<>("");

	/**
	 * Emits the request code text
	 */
	public ObservableField<String> requestCodeText = new ObservableField<>();

	/**
	 * Emits the name of the country
	 */
	public ObservableField<String> countryName = new ObservableField<>();

	public ObservableInt countryFlag = new ObservableInt();

	/**
	 * Emits duplicate of the country code
	 */
	public ObservableField<String> codeDuplicate = new ObservableField<>();

	/**
	 * Emits the verification code
	 */
	public ObservableField<String> verificationCode = new ObservableField<>("");

	public ObservableField<xxxEditTextValidationHelper> verificationCodeValidationHelper = new ObservableField<>(new xxxEditTextValidationHelper());

	private Context context;
	private LoginView view;

	private UserProfileRealmModel realmModel;

	private String androidId;
	private String androidName;

	CountryPhoneCode selectedCountry;

	Handler handler = new Handler();

	/**
	 * Runnable to check if user can request sms again
	 */
	private Runnable canSendSmsEvent = new Runnable() {
		@Override
		public void run() {
			if (handler != null) {
				handler.removeCallbacksAndMessages(null);
			}
			smsSentCheck();
		}
	};

	public LoginViewModel(LoginView view, UserProfileRealmModel realmModel) {
		this.view = view;
		this.realmModel = realmModel;
	}

	@Override
	public void start(Context ctx) {
		this.context = ctx;

		EventBus.getDefault().register(this);

		realmModel.start(ctx, null);

		phone.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
			@Override
			public void onPropertyChanged(Observable observable, int i) {
				String phone = ((ObservableField<String>) observable).get();
				isPhoneNumberValid.set(isPhoneNumberValid(phone));
				smsSentCheck();
			}
		});

		selectedCountry = getDefaultSelectedCountry();
		updatePhoneCode(selectedCountry);
	}

	/**
	 * Requests an sms to be sent
	 */
	public void onContinueClick() {
		if (view != null) {
			view.hideKeyboard();
		}

		if (context != null && !Utils.haveInternet(context) && view != null) {
			view.showInformer(context.getString(R.string.informer_app_no_connection));
			return;
		}

		requestSms(new LoginCaller.OnSmsSentCallback() {
			@Override
			public void onSent(RequestVerificationResp resp) {
				if (view != null) {
					view.hideProgressDialog();
					view.showVerificationScreen();
				}
			}

			@Override
			public void onError(ErrorResp resp) {
				if (view != null) {
					view.hideProgressDialog();

					if (resp.code != 10) {
						String message = Utils.getErrorMessage(resp, context.getApplicationContext());
						if (phone != null && message != null && view != null) {
							view.showInformer(message);
						}
					}
				}
			}
		});
	}

	public void onNavigationBackClick() {
		view.onBackPressed();
	}

	/**
	 * Starts an activity to select the country
	 */
	public void onSelectCountryClick() {
		if (view != null) {
			view.startSelectCountryScreen(selectedCountry.countryLetter);
		}
	}

	/**
	 * Starts code verification
	 */
	public void onVerifyClick() {
		if (view != null) {
			view.hideKeyboard();
		}

		if (context != null && !Utils.haveInternet(context) && view != null) {
			view.showInformer(context.getString(R.string.informer_app_no_connection));
			return;
		}

		if (view != null) {
			view.showProgressDialog();
		}
		verifyCode(verificationCode.get().trim(), new LoginCaller.OnVerificationCodeSentCallback() {

			@Override
			public void onSent(UserResp resp) {
				if (realmModel.isMainUserCreated()) {
					realmModel.stop();
					if (view != null) {
						view.hideProgressDialog();
						view.startChatListScreen();
						view.close();
					}
				} else {
					realmModel.stop();
					if (view != null) {
						view.hideProgressDialog();
						view.startMainProfileCreatingScreen();
					}
				}
			}

			@Override
			public void onError(ErrorResp error) {
				if (view != null && context != null) {
					view.hideProgressDialog();
					String message = Utils.getErrorMessage(error, context.getApplicationContext());
					view.showInformer(message);

					verificationCodeValidationHelper.get().showError(message);
				}
			}
		});
	}

	public void onEventMainThread(final SmsReceivedEvent event) {
		verificationCode.set(event.getCode());
		onVerifyClick();
	}

	public void onResendClick() {
		resendSms();
	}

	/**
	 * Verifies the entered code and creates profile if passed
	 *
	 * @param code entered verification code
	 */
	public void verifyCode(String code, @Nullable final LoginCaller.OnVerificationCodeSentCallback callback) {
		AnalyticManagerClient.getInstance().sendVerificationCodeRequested();

		getDeviceInfo();
		LoginCaller.sendVerificationCode(context, androidId, androidName, getFullPhoneNumber(), code,
				new LoginCaller.OnVerificationCodeSentCallback() {
					@Override

					public void onSent(UserResp resp) {
						if (resp != null) {
							realmModel.writeProfileToModel(resp.user, EnumConstants.PROFILE_TYPE_MAIN);

							if (resp.publicUser != null) {
								realmModel.writeProfileToModel(resp.publicUser, EnumConstants.PROFILE_TYPE_PUBLIC);
							}
						}
						if (callback != null) {
							callback.onSent(resp);
						}
					}

					@Override

					public void onError(ErrorResp error) {
						if (callback != null) {
							callback.onError(error);
						}
					}
				});
	}

	/**
	 * Sends sms again
	 */
	private void resendSms() {
		if (context != null && !Utils.haveInternet(context) && view != null) {
			view.showInformer(context.getString(R.string.informer_app_no_connection));
			return;
		}

		if (view != null) {
			view.showProgressDialog();
		}

		final RequestVerification requestVerification = new RequestVerification();
		requestVerification.phone = getFullPhoneNumber();
		requestVerification.deviceType = 1;
		zzzApi.sendIDRequest(RosterObjectHydrator.generateBytesToSendFromObject(requestVerification),
				new BaseAsyncCallback<RequestVerificationResp>() {
					@Override
					public void onErrorResp(ErrorResp error) {
						view.hideProgressDialog();

						if (error.code != 10) {
							String message = Utils.getErrorMessage(error, context);
							view.showInformer(message);
						}
					}

					@Override
					public void onSuccessResp(RequestVerificationResp result) {
						PreferenceHelper.setLastTimeSmsSend(requestVerification.phone, System.currentTimeMillis());
						handler.post(canSendSmsEvent);
						view.hideProgressDialog();
					}
				});
	}

	/**
	 * Checks if sms was sent or not
	 */
	private void smsSentCheck() {
		requestCodeText.set("");

		if (getPhoneWithoutCode().isEmpty()) {
			return;
		}

		long sendSmsCount = SEND_SMS_COUNT - (System.currentTimeMillis() - PreferenceHelper.getLastTimeSmsSend(getFullPhoneNumber()));
		if (sendSmsCount > 0) {
			long minutes = sendSmsCount / (60 * 1000);
			long seconds = (sendSmsCount / 1000) % 60;
			String sMinutes = String.valueOf(minutes);
			String sSeconds = String.valueOf(seconds);
			if (sSeconds.length() < 2) {
				sSeconds = "0" + sSeconds;
			}
			requestCodeText.set(context.getString(R.string.login_resend_sms_text, sMinutes, sSeconds));
			isCodeSent.set(true);
			handler.postDelayed(canSendSmsEvent, 1000);
		} else {
			isCodeSent.set(false);
			handler.removeCallbacksAndMessages(null);
		}
	}

	/**
	 * Requests to send sms
	 */
	private void requestSms(@Nullable final LoginCaller.OnSmsSentCallback callback) {
		yyyLinkApplication.isShowingBlockDialog = false;
		AnalyticManagerClient.getInstance().sendLogInCodeRequested();
		view.showProgressDialog();

		verificationCode.set("");

		if (!isCodeSent.get()) {

			final String phoneNumber = getFullPhoneNumber();

			LoginCaller.sendSmsRequest(context, phoneNumber, new LoginCaller.OnSmsSentCallback() {
				@Override
				public void onSent(RequestVerificationResp resp) {
					PreferenceHelper.setLastTimeSmsSend(phoneNumber, System.currentTimeMillis());
					handler.post(canSendSmsEvent);

					if (callback != null) {
						callback.onSent(resp);
					}
				}

				@Override
				public void onError(ErrorResp error) {
					if (callback != null) {
						callback.onError(error);
					}
				}
			});
		} else {
			if (view != null) {
				view.hideProgressDialog();
				view.showVerificationScreen();
			}
		}
	}

	/**
	 * Updates selected country to new value
	 *
	 * @param selectedCountry new selected country
	 */
	public void updatePhoneCode(CountryPhoneCode selectedCountry) {
		this.selectedCountry = selectedCountry;
		countryName.set(String.format("%s", selectedCountry.countryName));
		codeDuplicate.set(String.format("+%s", selectedCountry.code));
		countryFlag.set(Utils.getDrawableResourceIdByName(yyyLinkApplication.getyyyLinkApplication(), "country_" + selectedCountry.countryLetter.toLowerCase()));

		phone.set("");
		if (!TextUtils.isEmpty(selectedCountry.numberFormat)) {
			String[] parts = selectedCountry.numberFormat.split(" ");
			mask.set("");
			for (int i = 1; i < parts.length; i++) {
				mask.set(mask.get() + parts[i] + " ");
			}
			mask.set(mask.get().trim());

			mask.set(mask.get().replaceAll("X", "#"));
		} else {
			mask.set("###########");
		}
	}

	/**
	 * For handling activity's onActivityResult in ViewModel
	 */
	public void onActivityResult(Activity act, int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == ProfileCreateActivity.PROFILE_REQUEST_CODE) {
				if (view != null) {
					view.startChatListScreen();
					view.close();
				}
			} else if (requestCode == COUNTRY_REQUEST_CODE) {
				if (data != null) {
					CountryPhoneCode code = (CountryPhoneCode) data.getSerializableExtra(SelectCountryActivity.EXTRA_PHONE_CODE);
					if (!code.countryName.equals(selectedCountry.countryName))
						updatePhoneCode(code);
				}
			}
		}
	}

	/**
	 * Called when an action is being performed.
	 *
	 * @see android.widget.TextView.OnEditorActionListener
	 */
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (v.getId() == R.id.phone_number_edit_text) {
			if (actionId == EditorInfo.IME_ACTION_DONE && isPhoneNumberValid.get()) {
				onContinueClick();
			}
		} else if (v.getId() == R.id.edit_text_verify_code) {
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				onVerifyClick();
			}
		}

		return false;
	}

	/**
	 * Gets the information about device
	 */
	void getDeviceInfo() {
		androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
		androidName = Build.MANUFACTURER + " " + android.os.Build.MODEL;
		if (TextUtils.isEmpty(androidName)) {
			androidName = "Android Phone";
		}
	}

	/**
	 * Gets full phone number
	 *
	 * @return formatter full phone number
	 */
	private String getFullPhoneNumber() {
		if (selectedCountry.countryLetter.equals(UKRAINE_LETTER)) {
			selectedCountry.code = "380";
		}
		if (TextUtils.isEmpty(getPhoneWithoutCode()))
			return "";
		return String.format("+%s%s", selectedCountry.code, Long.valueOf(getPhoneWithoutCode()));
	}

	/**
	 * Gets phone number without country code
	 *
	 * @return phone without code
	 */
	private String getPhoneWithoutCode() {
		return phone.get().trim().replaceAll(" ", "");
	}

	public UserProfileRealmModel getRealmModel() {
		return realmModel;
	}

	/**
	 * Checks the phone number
	 *
	 * @param s phone number
	 * @return is number valid or not
	 */
	private boolean isPhoneNumberValid(String s) {
		if (TextUtils.isEmpty(s)) {
			return false;
		}

		String phone = s.trim().replaceAll(" ", "").replaceAll("X", "");
		boolean isLengthCorrect = phone.length() == mask.get().trim().replaceAll(" ", "").length();

		if (selectedCountry.countryLetter.equals(UKRAINE_LETTER)) {
			if (s.charAt(0) != '0') {
				return false;
			} else {
				phone = phone.substring(1, phone.length());
			}
		}
		if (!TextUtils.isEmpty(selectedCountry.numberFormat)) {
			return (!phone.startsWith("0")) && isLengthCorrect;
		} else {
			return !TextUtils.isEmpty(s);
		}
	}

	/**
	 * Gets default selected country
	 *
	 * @return default country
	 */
	private CountryPhoneCode getDefaultSelectedCountry() {
		CountryPhoneCode countryPhoneCode = new CountryPhoneCode();
		countryPhoneCode.code = "38";
		countryPhoneCode.countryLetter = "UA";
		countryPhoneCode.countryName = "Україна";
		countryPhoneCode.count = 12;
		countryPhoneCode.numberFormat = "38 XXX XXX XX XX";
		return countryPhoneCode;
	}

	/**
	 * Gets information about phone number on which verification code was sent
	 *
	 * @return formatted information with phone number
	 */
	public String getVerifyInfo() {
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
		phoneNumber.setCountryCode(Integer.valueOf(selectedCountry.code));
		try {
			phoneNumber.setNationalNumber(Long.valueOf(getPhoneWithoutCode()));
		} catch (NumberFormatException e) {
			Timber.e(e, e.getMessage());
		}
		String phoneValue = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

		return context.getString(R.string.enter_code);
	}

	public void onBackPressed(View v) {
		((Activity) v.getContext()).onBackPressed();
	}

	@Override
	public void stop() {
		EventBus.getDefault().unregister(this);
		realmModel.stop();
		context = null;
		handler.removeCallbacksAndMessages(null);
	}

}