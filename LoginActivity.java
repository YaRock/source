package xxx.core.screens.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import xxx.R;
import xxx.core.screens.main.MainScreenNavigationActivity;
import xxx.core.BaseActivity;
import xxx.core.helpers.Intents;
import xxx.core.helpers.SnackbarHelper;
import xxx.core.realm.zzzRealmHelper;
import xxx.core.screens.editprofile.ProfileCreateActivity;
import xxx.core.screens.welcome.WelcomeFragment;

public class LoginActivity extends BaseActivity implements LoginView {

    /**
     * ViewModel is the same for login and verification screens
     */
    private LoginViewModel viewModel;

    private View root;

    @Override
    public boolean needEventBus() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        root = findViewById(R.id.container);

        viewModel = new LoginViewModel(this, new UserProfileRealmModel(zzzRealmHelper.getUserConfiguration()));
        viewModel.start(this);

        boolean isLogout = false;
        if (getIntent().hasExtra(Intents.EXTRA_LOGOUT)) {
            isLogout = getIntent().getExtras().getBoolean(Intents.EXTRA_LOGOUT);
        }

        if (savedInstanceState == null) {
            if (isLogout) {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new LoginFragment())
                        .commit();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new WelcomeFragment())
                        .commit();
            }
        }
    }

    public LoginViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        viewModel.stop();
        super.onDestroy();
    }

    @Override
    public void startSelectCountryScreen(String selectedCountryLetter) {
        startActivityForResult(Intents.getSelectCountriesIntent(this, selectedCountryLetter),
                LoginViewModel.COUNTRY_REQUEST_CODE);
    }

    @Override
    public void startMainProfileCreatingScreen() {
        startActivityForResult(Intents.getProfileActivity(this, Intents.MAIN_PROFILE_CREATE), ProfileCreateActivity.PROFILE_REQUEST_CODE);
    }

    @Override
    public void startChatListScreen() {
//        startActivity(Intents.getRoomsListActivity(this, true));
        startActivity(new Intent(this, MainScreenNavigationActivity.class));
    }

    @Override
    public void showVerificationScreen() {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new VerificationFragment())
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    @Override
    public void showInformer(String text) {
        SnackbarHelper.fire(root, text, Snackbar.LENGTH_SHORT);
    }

    @Override
    public void hideKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            getCurrentFocus().clearFocus();
        }
    }

    @Override
    public void close() {
        finish();
    }

}
